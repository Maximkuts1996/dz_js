----------------makeProfileTimer
function doSomething(source) {
  var hash = 0;
  if (source.length === 0) return hash;
  for (var i = 0; i < source.length; i++) {
    var char = source.charCodeAt(i);
    hash = ((hash<<5)-hash)+char;
    hash = hash & hash;
  }
  return hash;
}
function makeProfileTimer() {
var t0 = performance.now();
var result = doSomething('Peter')
var t1 = performance.now();
return (t1 - t0).toFixed(4)
}
console.log('Took', makeProfileTimer());
---------------- Final Countdown
function sayHi(n) {
  if (n < 1) return console.log("Поехали!");
  console.log(n)
   return setTimeout(() => sayHi(n - 1), 1000)
}
 sayHi(5)
------myBind
function myBind(func, context , bindArgs) {
  function wrapper() {
    var args = Object.assign(bindArgs,arguments);
    return func.apply(context, args);
  }
  return wrapper;
}
var pow5 = myBind(Math.pow, Math, [undefined, 5])
var cube = myBind(Math.pow, Math, [undefined, 3]);
alert(pow5(2));//32
alert(cube(3));//27
var zeroPrompt = myBind(prompt, window, [undefined, "0"])
var someNumber = zeroPrompt("Введите число")
alert(someNumber);
------makeSaver
	function makeSaver(callable) {
    let res, executed = !1;
    return function() {
        return executed ? res : (executed = !0, res = callable());
    };
};
var saver = makeSaver(Math.random);
value1 = saver();
value2 = saver();
alert(value1 === value2);